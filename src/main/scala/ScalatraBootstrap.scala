import ski.pop.offer._
import org.scalatra._
import javax.servlet.ServletContext
import ski.pop.offer.repository.InMemoryRepository

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new OfferApi(new InMemoryRepository), OfferApi.contextPath)
  }
}
