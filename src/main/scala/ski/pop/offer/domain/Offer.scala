package ski.pop.offer.domain

import com.typesafe.scalalogging.StrictLogging

trait Offer {
  def id: Integer

  def `type`: String

  def expired: Boolean

  def price: String

  def currency: String

  def description: String
}

sealed case class ValidOffer(`type`: String = "offer_valid",
                             expired: Boolean = false,
                             price: String,
                             currency: String,
                             description: String,
                             id: Integer,
                             created: Long,
                             expires: Long) extends Offer {
  def isExpired: Boolean = {
    System.currentTimeMillis() > expires
  }
}

sealed case class ExpiredOffer(`type`: String = "offer_expired",
                               expired: Boolean = true,
                               price: String,
                               currency: String,
                               description: String,
                               id: Integer,
                               created: Long,
                               expires: Long) extends Offer

sealed case class CancelledOffer(`type`: String = "offer_cancelled",
                                 expired: Boolean = true,
                                 price: String,
                                 currency: String,
                                 description: String,
                                 id: Integer,
                                 created: Long,
                                 expires: Long) extends Offer

case class CreateOffer(expires: Long, description: String, price: String, currency: Option[String])

object OfferUtils extends StrictLogging {
  def create(id: Integer, create: CreateOffer): Offer = {
    val createdAt = System.currentTimeMillis()
    ValidOffer(id = id,
      created = createdAt,
      expires = createdAt + create.expires,
      price = create.price,
      currency = create.currency match {
        case Some(curr) => curr
        case None => "GBP"
      },
      description = create.description)
  }

  def toExpired(validOffer: ValidOffer): ExpiredOffer = {
    ExpiredOffer(
      id = validOffer.id,
      created = validOffer.created,
      expires = validOffer.expires,
      price = validOffer.price,
      currency = validOffer.currency,
      description = validOffer.description)
  }

  def toCancelled(validOffer: ValidOffer): CancelledOffer = {
    CancelledOffer(
      id = validOffer.id,
      created = validOffer.created,
      expires = validOffer.expires,
      price = validOffer.price,
      currency = validOffer.currency,
      description = validOffer.description
    )
  }

  def checkExpiry(offer: Offer): Option[Offer] = {
    offer match {
      case valid: ValidOffer =>
        if (valid.isExpired) {
          logger.debug("checkExpiry: Offer is expired")
          Some(toExpired(valid))
        } else {
          logger.debug("checkExpiry: Offer is not expired")
          None
        }
      case _ => None
    }
  }

}