package ski.pop.offer

import com.typesafe.scalalogging.StrictLogging
import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json._
import ski.pop.offer.domain.{CreateOffer, Offer}
import ski.pop.offer.repository.OfferRepository

object OfferApi {
  val contextPath = "/api/offer"
}

class OfferApi(repo: OfferRepository) extends ScalatraServlet with JacksonJsonSupport with StrictLogging {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }

  get("/:id") {
    val idOpt = params("id").toIntOption
    idOpt match {
      case Some(id) =>
        repo.getOffer(id) match {
          case Left(message) => NotFound(message)
          case Right(offer) => offer
        }
      case None => BadRequest("Id must be specified and be an integer")
    }
  }

  put("/:id") {
    val idOpt = params("id").toIntOption
    idOpt match {
      case Some(id) =>
        val offer = parsedBody.extract[Offer]
        if (id == offer.id) {
          repo.updateOffer(offer) match {
            case Left(message) => NotFound(message)
            case Right(offer) => offer
          }
        } else {
          BadRequest
        }
      case None => BadRequest("Id must be specified and be an integer")
    }
  }

  post("/") {
    val offer = parsedBody.extractOpt[CreateOffer]
    repo.createOffer(offer) match {
      case Left(message) => BadRequest(s"$message\nBody: ${request.body}")
      case Right(offer) => offer
    }
  }

  delete("/:id") {
    val idOpt = params("id").toIntOption
    idOpt match {
      case Some(id) =>
        repo.deleteOffer(id) match {
          case Left(message) => NotFound(message)
          case Right(offer) => offer
        }
      case None => BadRequest("Id must be specified and be an integer")
    }
  }

  put("/cancel/:id") {
    val idOpt = params("id").toIntOption
    logger.debug(s"$contextPath/cancel/:id | $idOpt")
    idOpt match {
      case Some(id) =>
        logger.debug(s"$contextPath/cancel/:id | Cancelling $id")
        repo.cancelOffer(id) match {
          case Left(message) => NotFound(message)
          case Right(offer) => offer
        }
      case None => BadRequest("Id must be specified and be an integer")
    }
  }

}
