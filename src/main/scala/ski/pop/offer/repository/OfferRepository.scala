package ski.pop.offer.repository

import com.typesafe.scalalogging.StrictLogging
import ski.pop.offer.domain.{CreateOffer, Offer, OfferUtils, ValidOffer}

trait OfferRepository {
  def updateOffer(offer: Offer): Either[String, Offer]

  def getOffer(id: Integer): Either[String, Offer]

  def createOffer(create: Option[CreateOffer]): Either[String, Offer]

  def deleteOffer(id: Integer): Either[String, Offer]

  def cancelOffer(id: Integer): Either[String, Offer]
}

class InMemoryRepository extends OfferRepository with StrictLogging {
  private var idTracker: Integer = 1
  private final var db: Map[Integer, Offer] = Map[Integer, Offer]()

  override def updateOffer(offer: Offer): Either[String, Offer] = {
    db.get(offer.id) match {
      case Some(off) =>
        db = db + (offer.id -> offer)
        Right(offer)
      case None =>
        Left("There was no offer to update")
    }
  }

  override def getOffer(id: Integer): Either[String, Offer] = {
    db.get(id) match {
      case Some(offer) =>
        logger.debug("getOffer: Found offer")
        OfferUtils.checkExpiry(offer) match {
          case Some(off) =>
            logger.debug(s"getOffer: Offer is expired updating it | $offer")
            updateOffer(off)
          case None =>
            logger.debug(s"getOffer: Offer is not expired | ${offer.expired}")
            Right(offer)
        }
      case None => Left("There is no offer to be found")
    }
  }

  override def createOffer(create: Option[CreateOffer]): Either[String, Offer] = {
    logger.debug("createOffer: CreateOffer=" + create)
    create match {
      case Some(o) =>
        val offer = OfferUtils.create(idTracker, o)
        logger.debug(s"createOffer: Offer=$offer")
        idTracker += 1
        db = db + (offer.id -> offer)
        Right(db(offer.id))
      case None =>
        Left("Malformed CreateOffer object basically")
    }
  }

  override def deleteOffer(id: Integer): Either[String, Offer] = {
    db.get(id) match {
      case Some(offer) =>
        db = db - (id)
        Right(offer)
      case None =>
        Left("There was no offer to delete")
    }

  }

  override def cancelOffer(id: Integer): Either[String, Offer] = {
    db.get(id) match {
      case Some(offer) =>
        offer match {
          case off: ValidOffer =>
            logger.debug(s"cancelOffer: Cancelling valid offer $off")
            updateOffer(OfferUtils.toCancelled(off))
          case _ => Left(s"${offer.`type`} cannot be cancelled")
        }
      case None =>
        Left("There is no offer to cancel")
    }
  }
}
