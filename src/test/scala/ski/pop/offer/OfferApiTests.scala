package ski.pop.offer

import org.json4s.{DefaultFormats, Formats}
import org.json4s.jackson.JsonMethods._
import org.scalatra.test.scalatest._
import ski.pop.offer.domain.{CancelledOffer, ExpiredOffer, Offer, ValidOffer}
import ski.pop.offer.repository.InMemoryRepository

import scala.io.Source

class OfferApiTests extends ScalatraFunSuite {
  implicit val jsonFormats: Formats = DefaultFormats
  addServlet(new OfferApi(new InMemoryRepository), "/*")

  test("POST with info creates an offer") {
    val postJson = Source.fromResource("post-create.json").mkString
    post("/", body = postJson) {
      status should equal(200)
      parse(body).extractOpt[ValidOffer] match {
        case Some(offer) =>
          assert(offer.id == 1)
          assert(offer.expires == (offer.created + 1L))
        case None => fail("Should contain an offer")
      }
    }
  }

  test("DELETE removes an offer") {
    val id = createAndReturnId()

    delete(s"/$id") {
      status should equal(200)
    }

    get(s"/$id") {
      status should equal(404)
    }
  }

  test("GET grabs the correct object") {
    val id = createAndReturnId()

    get(s"/$id") {
      status should equal(200)
      parse(body).extractOpt[ValidOffer] match {
        case Some(offer) =>
          assert(offer.id == id)
          assert(offer.description == "Description of my offer")
          assert(offer.price == "10.00")
        case None => fail("Should contain an offer")
      }
    }
  }

  test("POST then GET returns expired offer") {
    val id = createAndReturnId()

    get(s"/$id") {
      status should equal(200)
      parse(body).extractOpt[ExpiredOffer] match {
        case Some(offer) =>
          assert(offer.id == id)
          offer match {
            case o: ExpiredOffer =>
              assert(o.`type` == "offer_expired")
            case _ => fail()
          }
        case None => fail("Should be an expired offer")
      }
    }
  }

  test("Cancel offer") {
    val id = createAndReturnId()

    put(s"/cancel/$id") {
      status should equal(200)
      parse(body).extractOpt[CancelledOffer] match {
        case Some(offer) =>
          assert(offer.id == id)
          offer match {
            case o: CancelledOffer =>
              assert(o.`type` == "offer_cancelled")
            case _ => fail()
          }
        case None => fail("Should be an cancelled offer")
      }
    }
  }

  private def createAndReturnId(): Integer = {
    val postJson = Source.fromResource("post-create.json").mkString
    var id = 0
    post("/", body = postJson) {
      status should equal(200)
      parse(body).extractOpt[ValidOffer] match {
        case Some(offer) => id = offer.id
        case None => fail("Should be valid")
      }
    }
    id
  }

  override def header = ???
}
