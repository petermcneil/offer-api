# Offer API #

This API features a basic REST set up for persisting and expiring Offers.


To run this you'll need `sbt` installed.

https://www.scala-sbt.org/download.html


## Build & Run ##

```sh
$ sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.
